
var $loginMsg = $('.loginMsg'),
  $login = $('.login'),
  $signupMsg = $('.signupMsg'),
  $signup = $('.signup'),
  $frontbox = $('.frontbox');


$("#showModal").click(function() {
  $(".modal").addClass("is-active");
});

$(".modal-close").click(function() {
   $(".modal").removeClass("is-active");
});
$(".modal-background").click(function(){
  $(".modal").removeClass("is-active");
})

$('#switch1').on('click', function() {
  $loginMsg.toggleClass("visibility");
  $frontbox.addClass("moving");
  $signupMsg.toggleClass("visibility");

  $signup.toggleClass('hide');
  $login.toggleClass('hide');
})

$('#switch2').on('click', function() {
  $loginMsg.toggleClass("visibility");
  $frontbox.removeClass("moving");
  $signupMsg.toggleClass("visibility");

  $signup.toggleClass('hide');
  $login.toggleClass('hide');
})

setTimeout(function(){
  $('#switch1').click()
},1000)

setTimeout(function(){
  $('#switch2').click()
},3000)

$(document).ready(function(){
  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if (scroll > 100) {
      $(".topnavbar").css("background" , "rgb(48,48,63)");
    }

    else{
      $(".topnavbar").css("background" , "transparent");
    }
  })
})


$('.regform .stages label').click(function() {
  var radioButtons = $('.form input:radio');
  var selectedIndex = radioButtons.index(radioButtons.filter(':checked'));
  selectedIndex = selectedIndex + 1;
});

$('.regform button').click(function() {
  var radioButtons = $('.form input:radio');
  var selectedIndex = radioButtons.index(radioButtons.filter(':checked'));

  selectedIndex = selectedIndex + 2;

  $('.form input[type="radio"]:n' +
    'th-of-type(' + selectedIndex + ')').prop('checked', true);

  if (selectedIndex == 6) {
    $('button').html('Submit');
  }
});

$(document).ready(function(){
  $("#what").click(function() {
    $("#loginSection").animate({
      right: '-500px', opacity: '0',
    });
    $("#contentAreaBox").removeClass("is-7");
    $("#contentAreaBox").addClass("is-12");

  });

  $("#home").click(function(){
    $("#contentAreaBox").removeClass("is-12");
    $("#contentAreaBox").addClass("is-7");
    $("#loginSection").animate({
      right:'0',
      opacity:'1',
    });
  });
  $("#loginButton").click(function(){
    $("#loginBox").fadeIn();
    $("#loginButton").addClass("color");
    $("#registerButton").removeClass("color");
    $("#registrationBox").fadeOut();
    $("#whitebackground").animate({
      top:'0px',
    });
  });
  $("#registerButton").click(function(){
    $("#registerButton").addClass("color");
    $("#loginButton").removeClass("color");
    $("#registrationBox").fadeIn();
    $("#whitebackground").animate({
      top:'50px'
  });
  });
});
