import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService{
    constructor(){}

    public isAuthenticated() : boolean{
        const token =localStorage.getItem('access_token');
        if(token==null){
          return false;
        }
         return !this.isTokenExpired(token);
    }

    isTokenExpired(token?: string): boolean {
        const date = this.getTokenExpirationDate(token);
        if(date === undefined) return false;
        return !(date.valueOf() > new Date().valueOf());

      }

      getTokenExpirationDate(token: string): Date {
        const decoded = jwt_decode(token);

        if (decoded.exp === undefined) return null;

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp)
        return date;
      }



}
