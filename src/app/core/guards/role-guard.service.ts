import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, Router} from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    const expectedRole = route.data.expectedRole;

    const token = localStorage.getItem('access_token');

    if (token != null) {
      const tokenPayload = jwt_decode(token);
      console.log(tokenPayload.scopes[0]);
      if (
        this.authService.isTokenExpired(token) ||
        tokenPayload.scopes[0] != expectedRole
      ) {
        console.log(expectedRole);
        this.router.navigate(['']);
        return false;//should be kept to backward the request
      }
    }
    else {
      this.router.navigate(['']);
      return false;
    }
    return true;//should be kept if the page intended is to be load.

  }

}
