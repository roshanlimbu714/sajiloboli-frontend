import { TestBed, inject } from '@angular/core/testing';

import { LoginVerifierService } from './login-verifier.service';

describe('LoginVerifierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginVerifierService]
    });
  });

  it('should be created', inject([LoginVerifierService], (service: LoginVerifierService) => {
    expect(service).toBeTruthy();
  }));
});
