import {Injectable} from '@angular/core';

import {Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';

import {AuthService} from './auth.service';

import * as jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class LoginVerifierService implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate() {
        const token = localStorage.getItem('access_token');
        if (token != null) {
            if (!this.authService.isTokenExpired(token)) {

                console.log()
                const decoded = jwt_decode(token);
                if (decoded.scopes[0] == 'vendor') {
                    this.router.navigate(['vendor']);
                    return true;
                }
                else if (decoded.scopes[0] == 'customer') {
                    console.log('this is called');
                    this.router.navigate(['customer']);
                    return false;
                }
            }
            else {
                return true;
            }
        }
        return true;
    }
}
