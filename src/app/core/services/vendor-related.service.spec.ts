import { TestBed, inject } from '@angular/core/testing';

import { VendorRelatedService } from './vendor-related.service';

describe('VendorRelatedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VendorRelatedService]
    });
  });

  it('should be created', inject([VendorRelatedService], (service: VendorRelatedService) => {
    expect(service).toBeTruthy();
  }));
});
