import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Post} from '../../shared/models/Post';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
  })
}

@Injectable({
  providedIn: 'root'
})
export class EditPostService {

  private url = 'http://192.168.88.102:8000/api/';

  constructor(private http: HttpClient) {
  }

  editPost(post: Post) {
    return this.http.put(this.url + 'user/post', post, httpOptions);
  }
}
