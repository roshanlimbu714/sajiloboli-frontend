import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Post} from "../../shared/models/Post";



const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
  })
}
@Injectable({
  providedIn: 'root'
})
export class CreatePostService {

  private url = 'http://localhost:8000/api/';

  constructor(private http: HttpClient) {
  }

  createPost(post: Post) {
    return this.http.post(this.url + 'user/post', post, httpOptions);
  }
}
