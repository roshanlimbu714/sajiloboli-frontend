import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Bid} from '../../shared/models/Bid';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private url = 'http://localhost:8000/api/admin/';
  private url2 = 'http://localhost:8000/api/user/post/';
  getUnverifiedPosts() {
    return this.http.get(this.url + 'showUnverifiedPosts');
  }
  approvePosts(id: number) {
    return this.http.get(this.url + 'verifyPosts/' + id);
  }
  constructor(private http: HttpClient) { }
  getPost(id: number) {
    return this.http.get(this.url2 + id);
  }

  getBid(id: number) {
    return this.http.get(this.url2+ id + '/bid');
  }

  registerBid(id: number, bid: Bid) {
    console.log(bid)
    return this.http.post(this.url2 + id + '/bid', bid);
  }
}
