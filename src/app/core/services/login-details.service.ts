import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {RegistrationData} from "../../shared/models/LoginModel";

@Injectable({
    providedIn: 'root'
})


export class LoginDetailsService {
    public a;
    b: RegistrationData;
    subject = new BehaviorSubject(this.b);

    sendMessage(message: RegistrationData) {
        this.subject.next(message);
        console.log('subject message', this.subject.value);
        console.log('subject message', this.subject.getValue());
    }
    clearMessage() {
        this.subject.next(this.b);
    }
}
