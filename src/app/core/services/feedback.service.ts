import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  private url = 'http://localhost:8000/api/user/feedback';

  getfeedback() {
    return this.http.get(this.url);
  }

  constructor(private http: HttpClient) {
  }
}
