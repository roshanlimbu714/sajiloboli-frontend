import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserRequestService {

  private url = 'http://localhost:8000/api/admin/';

  constructor(private http: HttpClient) {
  }

  getUnverifiedUsers() {
    return this.http.get(this.url + 'showUnverifiedUsers');
  }

  approveUser(id: number) {
    return this.http.get(this.url + 'verifyUser/' + id);
  }

  rejectUser(id: number) {
    return this.http.get(this.url + 'rejectUser/' + id);
  }

  getAllUsers() {
    return this.http.get(this.url + 'showAllUsers');
  }

  getRejectedUsers() {
    return this.http.get(this.url + 'showRejectedUsers');
  }
}
