import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {VendorRegister} from '../../shared/models/LoginModel';
import {CustomerRegistration} from "../../shared/models/Post";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json', 'Accept': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private url2 = 'http://localhost:8000/api/';
  private url = 'http://localhost:8000/api/user/register';
  private url4 = 'http://localhost:8000/api/user/register';

  constructor(private http: HttpClient) {
  }
  vendorregister(register: VendorRegister) {
    console.log(register);
    return this.http.post(this.url, register);
  }
  getCategoryList() {
    return this.http.get(this.url2 + 'getAllCategory');
  }
  getTagsList() {
    return this.http.get(this.url2 + 'getAllTags');
  }
  customerregister(register: CustomerRegistration) {
    console.log(register);
    return this.http.post(this.url4, register, httpOptions);
  }
}
