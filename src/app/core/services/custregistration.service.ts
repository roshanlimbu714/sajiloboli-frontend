import { Injectable } from '@angular/core';
import {HttpClient , HttpHeaders} from '@angular/common/http';
import {CustomerRegistration} from "../../shared/models/Post";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type' : 'application/json' , 'Accept' : 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class CustomerRegisterService {

    private url4 = 'http://localhost:8101/api/user/register';

    customerregister(register: CustomerRegistration) {
        console.log(register);
        return this.http.post(this.url4, register, httpOptions);
    }
  constructor(private http: HttpClient) { }
}
