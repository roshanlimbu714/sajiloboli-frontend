import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Login, VendorRegister} from "../../shared/models/LoginModel";
import {UserRequest} from "../../shared/models/user.model";
// const httpOptions = {
//   headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
// };


@Injectable()
export class LoginRegistrationService {

  private url3 = 'http://localhost:8000/api/user/login';
  private url2 = 'http://localhost:8000/api/';
  private url = 'http://localhost:8000/api/user/register';

  constructor(private http: HttpClient) { }

  vendorregister(register: VendorRegister) {
    console.log(register);
    return this.http.post(this.url, register);
  }

  getCategoryList(){
    return this.http.get(this.url2+'getAllCategory');
  }
  getTagsList(){
    return this.http.get(this.url2+'getAllTags');
  }

  getAllDetails() {
    return this.http.get('http://localhost:8000/api/user');

  }

  login(login: Login) {
    return this.http.post(this.url3, login);
  }
}
