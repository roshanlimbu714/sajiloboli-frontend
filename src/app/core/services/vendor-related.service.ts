import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VendorRelatedService {
  private url = 'http://localhost:8000/api/user/';

  constructor(private http: HttpClient) {
  }

  getPostByUserCategory() {
    return this.http.get(this.url + 'list_post_by_user_category');
  }

  getPageinatedPosts(pageNumber: number) {
    console.log('in here in service');
    return this.http.get(this.url + 'list_post_by_user_category' + '?page=' + pageNumber);
  }


}
