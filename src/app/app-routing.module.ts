import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LandingpageComponent} from './modules/home/pages/landingpage/landingpage.component';
import {RegistrationComponent} from "./modules/registration/registration/registration.component";
import {LoginVerifierService} from "./core/guards/login-verifier.service";
import {CustomerRegistration} from "./shared/models/Post";
import {CustregistrationComponent} from "./modules/registration/custregistration/custregistration.component";
import {RecentComponent} from "./modules/home/pages/customer/recent/recent.component";
import {AdminComponent} from "./modules/home/pages/admin/admin.component";
import {HomeComponent} from "./modules/home/pages/home.component";
import {CreatePostComponent} from "./modules/home/pages/customer/create-post/create-post.component";
import {RoleGuardService} from "./core/guards/role-guard.service";
import {BidComponent} from "./modules/home/pages/vendor/bid/bid.component";
import {FaqComponent} from "./modules/home/components/faq/faq.component";
import {ProfileComponent} from "./modules/home/pages/profile/profile.component";
import {VendorComponent} from "./modules/home/pages/vendor/vendor.component";
import {CustomerComponent} from "./modules/home/pages/customer/customer.component";
import {PostComponent} from "./shared/post/post.component";


const routes: Routes = [
  {
    path: '', component: LandingpageComponent,
    canActivate: [LoginVerifierService]
  },
  {path: 'register', component: RegistrationComponent},
  {path: 'registration', component: CustregistrationComponent},
  {
    path: 'home', component: HomeComponent,
    canActivate: [LoginVerifierService]
  },
  {
    path: 'create', component: CreatePostComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'customer'
    }
  },
  { path: 'post/:id/bid', component: BidComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'vendor'
    }
  },
  {path: 'profile', component: ProfileComponent},
  {
    path: 'help', component: FaqComponent
    // children: [
    //   {path: 'customers', component: ForcustomerComponent},
    //   {path: 'vendors', component: ForVendorComponent},
    //   {path: 'general', component: ForGeneralComponent}
    // ]
  },
  {path: 'recent', component: RecentComponent},
  {
    path: 'post', component: PostComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'customer'
    }
  },
  {path: 'admin', component: AdminComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,
  ]
})
export class AppRoutingModule {
}
