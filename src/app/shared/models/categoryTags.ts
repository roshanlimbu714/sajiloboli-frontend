import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CategoryTagsService {
  listoftags: string[] = [];
  listofCatgories: string[] = [];

  tags = new BehaviorSubject(this.listoftags);
  categories = new BehaviorSubject(this.listofCatgories);

  sendTagsCategories(tags: any, categories: any) {
    this.tags.next(tags);
    this.categories.next(categories);
    console.log('Tags', this.tags.value);
    console.log('Categories', this.categories.value);
  }

  clearMessage() {
    this.tags.next(this.listoftags);
    this.categories.next(this.listofCatgories);
  }
}
