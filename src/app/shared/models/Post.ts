export class Post {
  public title?: string;
  public budget_from?: string;
  public budget_to?: string;
  public expiry_date?: string;
  public description?: string;
  public category?: any;
  public tags?: any = [];

  constructor(title?: string,
              budget_from?: string,
              budget_to?: string,
              expiry_date?: string,
              description?: string,
              category?: any,
              tags?: any,) {
    this.title = title ? title : null;
    this.budget_from = budget_from ? budget_from : null;
    this.budget_to = budget_to ? budget_to : null;
    this.expiry_date = expiry_date ? expiry_date : null;
    this.description = description ? description : null;
    this.category = category ? category : null;
    this.tags = tags ? tags : null;
  }
}
export class PostDetail {
  id?: any;
  title?: string;
  description?: string;
  budget_from?: number;
  budget_to?: number;
  status?: string;
  expiry_date?: string;
  category?: string;
  biddings?: any;
  tagname?: string[];

  constructor(id?: any,
              title?: string,
              description?: string,
              budget_from?: number,
              budget_to?: number,
              status?: string,
              expiry_date?: string,
              category?: string,
              biddings?: any,
              tagname?: string[]) {
    this.id = id ? id : null;
    this.title = title ? title : null;
    this.description = description ? description : null;
    this.budget_from = budget_from ? budget_from : null;
    this.budget_to = budget_to ? budget_to : null;
    this.status = status ? status : null;
    this.expiry_date = expiry_date ? expiry_date : null;
    this.category = category ? category : null;
    this.biddings = biddings ? biddings : null;
    this.tagname = tagname ? tagname : null;
  }
}
export class PostByCategory {
  public id?: number;
  public user_id?: number;
  public title?: string;
  public budget_from?: string;
  public budget_to?: string;
  public expiry_date?: string;
  public description?: string;
  public category?: any;
  public tags?: any = [];
  public biddings?: any;
  public updated_at?: any[];

  constructor(id?: number,
              user_id?: number,
              title?: string,
              budget_from?: string,
              budget_to?: string,
              expiry_date?: string,
              description?: string,
              category?: any,
              tags?: any,
              biddings?: any,
              updated_at?: any[]) {
    this.id = id ? id : null;
    this.user_id = user_id ? user_id : null;
    this.title = title ? title : null;
    this.budget_from = budget_from ? budget_from : null;
    this.budget_to = budget_to ? budget_to : null;
    this.expiry_date = expiry_date ? expiry_date : null;
    this.description = description ? description : null;
    this.category = category ? category : null;
    this.tags = tags ? tags : null;
    this.biddings = biddings ? biddings : null;
    this.updated_at = updated_at ? updated_at : null;
  }
}

export class PostRequest {
  public id?: number;
  public title?: string;
  public budget_from?: number;
  public budget_to?: number;
  public description?: any;
  public status?: string;
  public expiry_date?: number;
  public category?: string;
  public tags?: any;
  public updated_at?: any;

  constructor(id?: number,
              title?: string,
              budget_from?: number,
              budget_to?: number,
              description?: any,
              status?: string,
              expiry_date?: number,
              category?: string,
              tags?: any,
              updated_at?: any) {
    this.id = id ? id : null;
    this.title = title ? title : null;
    this.budget_from = budget_from ? budget_from : null;
    this.budget_to = budget_to ? budget_to : null;
    this.description = description ? description : null;
    this.status = status ? status : null;
    this.expiry_date = expiry_date ? expiry_date : null;
    this.category = category ? category : null;
    this.tags = tags ? tags : null;
    this.updated_at = updated_at ? updated_at : null;
  }
}

export class CustomerRegistration {
  public email?: string;
  public name?: string;
  public password?: string;
  public c_password?: string;
  public rolename ?: string;
  public documenttypename?: string;
  public dob?: string;
  public firstname?: string;
  public lastname?: string;
  public gender?: string;
  public permanentaddress?: string;
  public phone?: string;

  constructor(email?: string,
              name?: string,
              password?: string,
              c_password?: string,
              rolename ?: string,
              documenttypename?: string,
              dob?: string,
              firstname?: string,
              lastname?: string,
              gender?: string,
              permanentaddress?: string,
              phone?: string) {
    this.email = email ? email : null;
    this.name = this.firstname ? this.firstname : null;
    this.password = password ? password : null;
    this.c_password = c_password ? c_password : null;
    this.rolename = rolename ? rolename : null;
    this.documenttypename = documenttypename ? documenttypename : null;
    this.dob = dob ? dob : null;
    this.firstname = firstname ? firstname : null;
    this.lastname = lastname ? lastname : null;
    this.gender = gender ? gender : null;
    this.permanentaddress = permanentaddress ? permanentaddress : null;
    this.phone = phone ? phone : null;
  }
}


