export class Login {
  constructor(public email?: string,
              public password?: string) {
  }
}

export class RegistrationData {
  constructor(public email?: string,
              public password?: string,
              public cpassword?: string,
              public usertype?: string) {
  }
}

export class VendorRegister {
  public email?: string;
  public name?: string;
  public password?: string;
  public c_password?: string;
  public rolename ?: string;
  public documenttypename ?: string;
  public doe?: string;
  public firstname?: string;
  public lastname?: string;
  public gender?: string;
  public permanentaddress?: string;
  public phone?: string;
  public companyname?: string;
  public tagline?: string;
  public description?: string;
  public temporaryaddress      ?: string;
  public companycontactNumber ?: string;
  public optionalphone ?: string;
  public tags?: any = [];
  public category?: string;

  constructor(email?: string,
              name?: string,
              password?: string,
              c_password?: string,
              rolename ?: string,
              documenttypename?: string,
              doe?: string,
              firstname?: string,
              lastname?: string,
              gender?: string,
              permanentaddress?: string,
              phone?: string,
              companyname?: string,
              tagline?: string,
              description?: string,
              temporaryaddress?: string,
              optionalphone?: string,
              tags ?: any,
              category ?: string,) {
    this.email = email ? email : null;
    this.name = this.firstname ? this.firstname : null;
    this.password = password ? password : null;
    this.c_password = c_password ? c_password : null;
    this.rolename = rolename ? rolename : null;
    this.documenttypename = documenttypename ? documenttypename : null;
    this.doe = doe ? doe : null;
    this.firstname = firstname ? firstname : null;
    this.lastname = lastname ? lastname : null;
    this.gender = gender ? gender : null;
    this.companyname = companyname ? companyname : null;
    this.tagline = tagline ? tagline : null;
    this.description = description ? description : null;
    this.permanentaddress = permanentaddress ? permanentaddress : null;
    this.temporaryaddress = temporaryaddress ? temporaryaddress : null;
    this.phone = phone ? phone : null;
    this.optionalphone = optionalphone ? optionalphone : null;
    this.tags = tags ? tags : null;
    this.category = category ? category : null;
  }
}
