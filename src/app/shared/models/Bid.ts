export class Bid {
  id?: number;
  bid_amount?: number;
  bid_details?: number;
  user_id?: number;
  post_id?: number;
  created_at?: Date;
  updated_at?: Date;
  quantity?: number;
  quality?: string;
  delivery_date?: Date;

  constructor(id?: number,
              bid_amount?: number,
              bid_details?: number,
              user_id?: number,
              post_id?: number,
              created_at?: Date,
              updated_at?: Date,
              quantity?: number,
              quality?: string,
              delivery_date?: Date) {
    this.id = id ? id : null;
    this.bid_amount = bid_amount ? bid_amount : null;
    this.bid_details = bid_details ? bid_details : null;
    this.user_id = user_id ? user_id : null;
    this.post_id = post_id ? post_id : null;
    this.created_at = created_at ? created_at : null;
    this.updated_at = updated_at ? updated_at : null;
    this.quantity = quantity ? quantity : null;
    this.quality = quality ? quality : null;
    this.delivery_date = delivery_date ? delivery_date : null;
  }
}
