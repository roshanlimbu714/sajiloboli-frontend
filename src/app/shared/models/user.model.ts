export class ContactDetail {
  public homephone?: string;
  public optionalphone?: string;
  public permanentaddress?: string;
  public phone?: string;
  public temporaryaddress?: string;

  constructor(homephone?: string,
              optionalphone?: string,
              permanentaddress?: string,
              phone?: string,
              temporaryaddress?: string) {
    this.homephone = homephone ? homephone : null;
    this.optionalphone = optionalphone ? optionalphone : null;
    this.permanentaddress = permanentaddress ? permanentaddress : null;
    this.phone = phone ? phone : null;
    this.temporaryaddress = temporaryaddress ? temporaryaddress : null;
  }
}

export class MemberDetail {
  public company_detail?: any;
  public contact?: ContactDetail;
  public dob?: string;
  public firstname?: string;
  public gender?: string;
  public lastname?: string;

  constructor(company_detail?: any,
              contact?: ContactDetail,
              dob?: string,
              firstname?: string,
              gender?: string,
              lastname?: string) {
    this.company_detail = company_detail ? company_detail : null;
    this.contact = contact ? new ContactDetail: null;
    this.dob = dob ? dob : null;
    this.firstname = firstname ? firstname : null;
    this.gender = gender ? gender : null;
    this.lastname = firstname ? firstname : null;

  }

}

export class UserRequest {
  public id?: number;
  public name?: string;
  public email?: string;
  public role?: any;
  public member_detail?: MemberDetail;
  public emailVerified?: boolean;
  public createdAt?: any;

  constructor(id?: number,
              name?: string,
              email?: string,
              role?: any,
              member_detail?: MemberDetail,
              emailVerified?: boolean,
              createdAt?: any) {
    this.id = id ? id : null;
    this.name = name ? name : null;
    this.email = email ? email : null;
    this.role = role ? role : null;
    this.member_detail = member_detail ? new MemberDetail : null;
    this.emailVerified = emailVerified ? emailVerified : null;
    this.createdAt = createdAt ? createdAt : null;

  }
}
