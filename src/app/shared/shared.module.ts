import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotificationComponent} from './notification/notification.component';
import {MaterialModule} from "./material/material.module";
import {PostComponent} from "./post/post.component";

@NgModule({
  declarations: [
    NotificationComponent,
    PostComponent],
  imports: [
    CommonModule,
    MaterialModule,

  ],
  exports: [],
  entryComponents: [
    NotificationComponent
  ]
})
export class SharedModule {
}
