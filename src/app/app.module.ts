import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './shared/material/material.module';
import {HomeModule} from './modules/home/home.module';
import {LoginDetailsService} from './core/services/login-details.service';
import {LoginRegistrationService} from './core/services/login-registration.service';
import {RegistrationModule} from './modules/registration/registration.module';
import {SharedModule} from './shared/shared.module';
import {FormsModule} from "@angular/forms";
import {NavbarComponent} from "./modules/home/components/navbar/navbar.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    HomeModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    RegistrationModule,
    SharedModule,

  ],
  exports: [
    MaterialModule,
    NavbarComponent,
  ],
  providers: [
    LoginDetailsService,
    LoginRegistrationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
