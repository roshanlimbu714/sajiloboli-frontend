import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';
import {DatePipe} from '@angular/common';
import { LoginDetailsService} from '../../../core/services/login-details.service';
import {CustomerRegisterService} from '../../../core/services/custregistration.service';
import {CustomerRegistration} from '../../../shared/models/Post';
import {RegistrationData} from '../../../shared/models/LoginModel';
import {Router} from "@angular/router";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-custregistration',
  templateUrl: './custregistration.component.html',
  styleUrls: ['./custregistration.component.scss']
})
export class CustregistrationComponent implements OnInit {
  userModel: CustomerRegistration;
  panelOpenState = false;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  one: any = 'widout submit click';
  matcher = new MyErrorStateMatcher();

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  startDate = new Date(1990, 0, 1);
  loginDetails: RegistrationData;
  listOfTags: any = [];
  gender: string;
  agreed: boolean = false;
  datePipe: any;

  // pipe = new DatePipe('en-US');

  pushTag(tag) {
    this.listOfTags.push(tag);
    console.log(tag);
  }

  remove(tag: any): void {
    let tagindex: number = this.listOfTags.indexOf(tag);
    if (tagindex > -1) {
      this.listOfTags.splice(tagindex, 1);
    }
  }

  constructor(private _formBuilder: FormBuilder,
              private registerService: CustomerRegisterService,
              private messageService: LoginDetailsService,
              private crudService: CustomerRegisterService,
              private router: Router) {
  }

  displayOne() {
    this.userModel.name = this.firstFormGroup.controls['firstName'].value;
    this.userModel.firstname = this.firstFormGroup.controls['firstName'].value;
    this.userModel.lastname = this.firstFormGroup.controls['lastName'].value;
    this.userModel.permanentaddress = this.firstFormGroup.controls['permanentAddress'].value;
    this.userModel.phone = this.firstFormGroup.controls['phone'].value;
    this.userModel.dob = this.transform(this.firstFormGroup.controls['dob'].value);
    this.userModel.gender = this.firstFormGroup.controls['gender'].value;
    this.userModel.email = this.loginDetails.email;
    this.userModel.password = this.loginDetails.password;
    this.userModel.c_password = this.loginDetails.cpassword;
    this.userModel.rolename = this.loginDetails.usertype;
    // this.a = this.pipe.transform(this.userModel.dob,'short');
    console.log(this.userModel);
    this.crudService.customerregister(this.userModel).subscribe(res => (res), () => console.log('error'));
  }

  transform(value: any) {
    this.datePipe = new DatePipe('en-US');
    value = this.datePipe.transform(value, 'yyyy-MM-dd');
    return value;
  }

  // onSuccess(res) {
  //     this.crudService = res.json();
  //     console.log(res.json());
  // }

  onSubmit() {
    this.registerService.customerregister(this.userModel)
      .subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );
    localStorage.setItem('userType', this.userModel.rolename);
    this.router.navigate(['home']);
  }

  displayValue() {
    console.log(this.agreed);
  }


  ngOnInit() {
    this.userModel = {};
    this.messageService.subject.subscribe((msg) => {
      this.loginDetails = msg;
      console.log('message', msg);
    });
    console.log(this.userModel);
    this.firstFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dob: ['', Validators.required],
      permanentAddress: ['', Validators.required],
      phone: ['', Validators.required],
      gender: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      documenttypename: ['', Validators.required]
    });
  }
}
