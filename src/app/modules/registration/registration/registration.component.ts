import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatAutocompleteSelectedEvent, MatChipInputEvent, MatSnackBar} from '@angular/material';

import {DatePipe} from '@angular/common';
import {Observable} from 'rxjs/index';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {map, startWith} from 'rxjs/internal/operators';
import {RegistrationData, VendorRegister} from '../../../shared/models/LoginModel';
import {LoginDetailsService} from '../../../core/services/login-details.service';
import {Router} from '@angular/router';
import {RegisterService} from '../../../core/services/register.service';
import {NotificationComponent} from '../../../shared/notification/notification.component';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  visible = true;
  userModel: VendorRegister;
  panelOpenState = false;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  categoryCtrl = new FormControl();
  tagCtrl = new FormControl();

  one: any = 'without submit click';
  matcher = new MyErrorStateMatcher();
  listoftags: string[] = [];
  listofcategories: string[] = [];
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  startDate = new Date(1990, 0, 1);
  loginDetails: RegistrationData;
  tags: any = [];
  gender: string;
  agreed: boolean = false;
  datePipe: any;
  // pipe = new DatePipe('en-US');
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredCategories: Observable<string[]>;
  filteredTags: Observable<string[]>;


  @ViewChild('categoryInput') categoryInput: ElementRef<HTMLInputElement>;
  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  add(events: MatChipInputEvent): void {
    const input = events.input;
    const value = events.value;
    // Add our tag
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.tagCtrl.setValue(null);
    console.log('tags', this.tags);
  }


  remove(tag: string): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }


  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.viewValue);
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
    console.log('selectedTags', this.tags);
  }


  private _filter(value: string): string[] {
    const filterValues = value.toLowerCase();
    return this.listoftags.filter(tag => tag.toLowerCase().indexOf(filterValues) === 0);
  }

  constructor(public router: Router,
              public notify: MatSnackBar,
              private _formBuilder: FormBuilder,
              private registerService: RegisterService,
              private messageService: LoginDetailsService,
              private crudService: RegisterService) {

  }

  displayTwo() {
    this.userModel.name = this.firstFormGroup.controls['firstname'].value;
    this.userModel.firstname = this.firstFormGroup.controls['firstname'].value;
    this.userModel.lastname = this.firstFormGroup.controls['lastname'].value;
    this.userModel.gender = this.firstFormGroup.controls['gender'].value;
    this.userModel.email = this.loginDetails.email;
    this.userModel.password = this.loginDetails.password;
    this.userModel.c_password = this.loginDetails.cpassword;
    this.userModel.rolename = this.loginDetails.usertype;
    this.userModel.companyname = this.secondFormGroup.controls['companyname'].value;
    this.userModel.tagline = this.secondFormGroup.controls['tagline'].value;
    this.userModel.description = this.secondFormGroup.controls['companydescription'].value;
    this.userModel.permanentaddress = this.secondFormGroup.controls['companylocation'].value;
    this.userModel.phone = this.secondFormGroup.controls['contactnumber'].value;
    this.userModel.optionalphone = this.secondFormGroup.controls['companyoptionalnumber'].value;
    this.userModel.tags = this.tags;
    this.userModel.category = this.thirdFormGroup.controls['categoryCtrl'].value;
    console.log(this.userModel.category);
    this.userModel.doe = this.transform(this.secondFormGroup.controls['doe'].value);
    console.log(this.userModel);
    this.crudService.vendorregister(this.userModel).subscribe(res => {
      this.openNotification();
      this.router.navigate(['']);
    }, (err) => console.log(err));

  }

  transform(value: any) {
    this.datePipe = new DatePipe('en-US');
    value = this.datePipe.transform(value, 'yyyy-MM-dd');
    return value;
  }

  // onSuccess(res) {
  //     this.crudService = res.json();
  //     console.log(res.json());
  // }

  openNotification() {
    this.notify.openFromComponent(NotificationComponent, {
      data: 'You have been registered !! check your email to verify',
      duration: 3000,
    });
  }

  onSubmit() {
    this.registerService.vendorregister(this.userModel)
      .subscribe(
        (res) => console.log(res),
        (err) => console.log(err)
      );
    localStorage.setItem('userType', this.userModel.rolename);
    this.router.navigate(['home']);

  }

  displayValue() {
    console.log(this.agreed);
  }

  loadLoadAllTags() {
    this.registerService.getTagsList().subscribe(
      (res) => {
        this.listoftags = Array.from(new Set(res['data']));
        console.log(this.listoftags);
      });
  }

  loadLoadAllCategories() {
    this.registerService.getCategoryList().subscribe(
      (res) => {
        this.listofcategories = Array.from(new Set(res['data']));
        console.log(this.listofcategories);
      });
  };

  ngOnInit() {
    this.userModel = {};
    this.messageService.subject.subscribe((msg) => {
      this.loginDetails = msg;
      console.log('LoginDetails', msg);
    });
    this.loadLoadAllCategories();
    this.loadLoadAllTags();
    console.log(this.userModel);
    this.firstFormGroup = this._formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      gender: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      companyname: ['', Validators.required],
      tagline: ['', Validators.required],
      companydescription: ['', Validators.required],
      position: ['', Validators.required],
      companylocation: ['', Validators.required],
      contactnumber: ['', Validators.required],
      companyoptionalnumber: ['', Validators.required],
      doe: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      categoryCtrl: ['', Validators.required],
      tagCtrl: ['', Validators.required],

    });
    this.fourthFormGroup = this._formBuilder.group({});

  }
}
