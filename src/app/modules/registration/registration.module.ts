import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegistrationComponent} from './registration/registration.component';
import {CustregistrationComponent} from './custregistration/custregistration.component';
import {MaterialModule} from '../../shared/material/material.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoginDetailsService} from "../../core/services/login-details.service";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    CustregistrationComponent,
    RegistrationComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    MaterialModule,
    SharedModule
  ],
  providers: [
    LoginDetailsService
  ]
})
export class RegistrationModule {
}
