import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FaqComponent} from './components/faq/faq.component';
import {LandingpageModule} from './pages/landingpage/landingpage.module';
import {CustomerModule} from './pages/customer/customer.module';
import {VendorModule} from './pages/vendor/vendor.module';
import {MaterialModule} from "../../shared/material/material.module";
import {AdminModule} from "./pages/admin/admin.module";
import {ProfileComponent} from "./pages/profile/profile.component";
import {HomeComponent} from "./pages/home.component";



@NgModule({
  declarations: [
    FaqComponent,
    ProfileComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    CustomerModule,
    VendorModule,
    LandingpageModule,
    MaterialModule,
    AdminModule,
  ],
  exports: [
    CustomerModule,
    VendorModule,
    LandingpageModule,
    MaterialModule,
    AdminModule,

  ]
})
export class HomeModule {
}
