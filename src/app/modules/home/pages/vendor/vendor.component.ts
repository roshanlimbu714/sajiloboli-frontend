import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {Observable} from "rxjs";
import {MatAutocompleteSelectedEvent, MatChipInputEvent} from "@angular/material";
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {PostByCategory} from "../../../../shared/models/Post";
import {VendorRelatedService} from "../../../../core/services/vendor-related.service";
import {RegisterService} from "../../../../core/services/register.service";


@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.scss']
})
export class VendorComponent implements OnInit {
  tags: any = [];
  categories: any = [];
  listoftags: string[] = [];
  listofcategories: string[] = [];
  postArray: PostByCategory[];
  filterGroup: FormGroup;
  categoryCtrl = new FormControl();
  tagCtrl = new FormControl();
  show: any = 1;
  firstPage: number;
  nextPage: number;
  currentPage: number;
  lastPage: number;
  previousPage: number;
  // pipe = new DatePipe('en-US');
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredCategories: Observable<string[]>;
  filteredTags: Observable<string[]>;


  @ViewChild('categoryInput') categoryInput: ElementRef<HTMLInputElement>;
  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  constructor(private vendorService: VendorRelatedService,
              private categoriesService: RegisterService,
              private _formBuilder: FormBuilder) {
  }


  add(events: MatChipInputEvent): void {
    const input = events.input;
    const value = events.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.tagCtrl.setValue(null);
    console.log('tags', this.tags);
  }
  addCat(events: MatChipInputEvent): void {
    const input = events.input;
    const value = events.value;
    if ((value || '').trim()) {
      this.categories.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
    this.categoryCtrl.setValue(null);
    console.log(this.categories);
  }
  remove(tag: string): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }
  removeCat(category: string): void {
    const index = this.categories.indexOf(category);
    if (index >= 0) {
      this.categories.splice(index, 1);
    }
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.viewValue);
    this.tagInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }
  selectedCat(event: MatAutocompleteSelectedEvent): void {
    this.categories.push(event.option.viewValue);
    this.categoryInput.nativeElement.value = '';
    this.categoryCtrl.setValue(null);
  }
  private _filter(value: string): string[] {
    const filterValue = value;
    return this.listoftags.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filter2(value: string): string[] {
    const filterValue = value;
    return this.listofcategories.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  loadLoadAllTags() {
    this.categoriesService.getTagsList().subscribe(
      (res) => {
        this.listoftags = res['data'];
      });
    this.filteredTags = this.tagCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  loadLoadAllCategories() {
    this.categoriesService.getCategoryList().subscribe(
      (res) => {
        this.listofcategories = res['data'];
      });
    this.filteredCategories = this.categoryCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter2(value))
    );
  }


  loadAllPostByCategory() {
    console.log("here");
    this.vendorService.getPostByUserCategory().subscribe(
      (res) => {
        console.log(res);
        this.postArray = res['data'];
        this.currentPage = res['meta']['current_page'];
        this.firstPage = this.currentPage;
        this.nextPage = this.currentPage + 1;
        this.lastPage = res['meta']['last_page'];
        this.show = 0;
        console.log(this.currentPage);
        console.log(this.lastPage);
        console.log(this.nextPage);
        console.log(this.postArray[0]);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  pageInate(pageNumber: number) {
    console.log(pageNumber);
    this.vendorService.getPageinatedPosts(pageNumber).subscribe(
      (res) => {
        this.show = 1;
        this.postArray.push.apply(this.postArray, res['data']);
        this.show = 0;
        this.previousPage = this.currentPage;
        this.currentPage = res['meta']['current_page'];
        this.nextPage = this.currentPage + 1;
        this.lastPage = res['meta']['last_page'];
        console.log(this.postArray[0]);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    // this.postArray={};;
    this.loadLoadAllCategories();
    this.loadLoadAllTags();
    this.loadAllPostByCategory();
   }
}
