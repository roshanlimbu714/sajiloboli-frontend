import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidOnPostComponent } from './bid-on-post.component';

describe('BidOnPostComponent', () => {
  let component: BidOnPostComponent;
  let fixture: ComponentFixture<BidOnPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidOnPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidOnPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
