import {Component, OnInit, Inject} from '@angular/core';
import {DatePipe} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {Bid} from "../../../../../../shared/models/Bid";
import {PostDetail} from "../../../../../../shared/models/Post";
import {ActivatedRoute} from "@angular/router";
import {PostService} from "../../../../../../core/services/post.service";
import {NotificationComponent} from "../../../../../../shared/notification/notification.component";

@Component({
  selector: 'app-bid-on-post',
  templateUrl: './bid-on-post.component.html',
  styleUrls: ['./bid-on-post.component.scss']
})
export class BidOnPostComponent implements OnInit {
  id: number;
  post: PostDetail;
  bid: Bid;
  datePipe: any;

  constructor(public notify: MatSnackBar, public dialogRef: MatDialogRef<BidOnPostComponent>, @Inject(MAT_DIALOG_DATA)
  public data: number, private route: ActivatedRoute, private postDetailService: PostService) {
  }

  transform(value: any) {
    this.datePipe = new DatePipe('en-US');
    value = this.datePipe.transform(value, 'yyyy-MM-dd');
    return value;
  }

  ngOnInit() {
    // this.post={};
    this.bid = {};
    this.route.params.subscribe(params => {
      this.id = this.data;   //<----- + sign converts string value to number
    });


    console.log(this.id + "this i think is problem");
    //  this.getPostDetail();
  }

  // getPostDetail(){
  //   this.postDetailService.getPost(this.id).subscribe(
  //     (res)=>{this.post=res['data']
  //   console.log(this.post)},
  //     (err)=>{console.log(err)}
  //   );
  // }

  onSubmit() {
    console.log(this.bid);
    this.bid.delivery_date = this.transform(this.bid.delivery_date);
    this.postDetailService.registerBid(this.id, this.bid).subscribe(
      (res) => {
        console.log(res)
      },
      (err) => {
        console.log(err)
      }
    );
    this.openNotification();
    this.dialogRef.close();

  }

  openNotification() {
    this.notify.openFromComponent(NotificationComponent, {
      data: 'Your bid has been posted',
      duration: 500,
    });
  }

}
