import {Component, OnInit} from '@angular/core';
import {BidOnPostComponent} from './bid-on-post/bid-on-post.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Sort} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {Bid} from '../../../../../shared/models/Bid';
import {PostDetail} from '../../../../../shared/models/Post';
import {PostService} from '../../../../../core/services/post.service';

@Component({
  selector: 'app-bid',
  templateUrl: './bid.component.html',
  styleUrls: ['./bid.component.scss']
})


export class BidComponent implements OnInit {
  id: number;
  post: PostDetail;
  bid: Bid;
  bidArray: Bid[];
  sortedData: Bid[];


  constructor(private route: ActivatedRoute, private postDetailService: PostService, public dialog: MatDialog) {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(BidOnPostComponent, {
      data: this.id,
    });
  }


  ngOnInit() {
    this.post = {};
    this.bid = {};
    this.route.params.subscribe(params => {
      this.id = +params['id'];   //<----- + sign converts string value to number
    });

    console.log(this.id);
    this.getPostDetail();
    this.getBids();
  }

  getPostDetail() {
    this.postDetailService.getPost(this.id).subscribe(
      (res) => {
        this.post = res['data']
        console.log(this.post)
      },
      (err) => {
        console.log(err)
      }
    );

  }

  // onSubmit(){
  //   console.log(this.bid);
  //   this.postDetailService.registerBid(this.id,this.bid).subscribe(
  //     (res)=>{console.log(res)},
  //     (err)=>{console.log(err)}
  //   );

  // }
  getBids() {
    this.postDetailService.getBid(this.id).subscribe(
      (res) => {
        console.log(res);
        this.bidArray = res['data'];
        console.log(this.bid)
      },
      (err) => {
        console.log(err)
      }
    );
  }


  sortData(sort: Sort) {
    const data = this.bidArray.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'bid_amount':
          return this.compare(a.bid_amount, b.bid_amount, isAsc);
        case 'quantity':
          return this.compare(a.quantity, b.quantity, isAsc);
        case 'quality':
          return this.compare(a.quality, b.quality, isAsc);
        case 'delivery_date':
          return this.compare(a.delivery_date, b.delivery_date, isAsc);
        case 'updated_at':
          return this.compare(a.updated_at, b.updated_at, isAsc);
        default:
          return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


}
