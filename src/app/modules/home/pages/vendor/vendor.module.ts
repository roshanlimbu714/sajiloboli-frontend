import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "../../../../shared/material/material.module";
import {VendorComponent} from "./vendor.component";
import {BidOnPostComponent} from "./bid/bid-on-post/bid-on-post.component";
import {BidComponent} from "./bid/bid.component";
import {VpostComponent} from "./vpost/vpost.component";
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    VendorComponent,
    BidOnPostComponent,
    BidComponent,
    VpostComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ],
  exports: [
    VendorComponent,
    MaterialModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class VendorModule {
}
