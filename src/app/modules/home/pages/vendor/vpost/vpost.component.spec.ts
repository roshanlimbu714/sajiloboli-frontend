import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VpostComponent } from './vpost.component';

describe('VpostComponent', () => {
  let component: VpostComponent;
  let fixture: ComponentFixture<VpostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VpostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
