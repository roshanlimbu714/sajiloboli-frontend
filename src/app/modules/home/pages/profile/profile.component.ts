import {Component, OnInit} from '@angular/core';
import {ContactDetail, UserRequest} from "../../../../shared/models/user.model";
import {LoginRegistrationService} from "../../../../core/services/login-registration.service";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userDetail: UserRequest[] = [];
  contactDetail: ContactDetail[] = [];

  constructor(private userInfo: LoginRegistrationService) {
  }

  getAllUserDetails() {
    this.userInfo.getAllDetails().subscribe(
      (res) => {
        this.userDetail = res['data'];
        this.contactDetail = res['data'].member_detail.contact;
      }
    );
  }

  ngOnInit() {
    this.getAllUserDetails();
  }

}
