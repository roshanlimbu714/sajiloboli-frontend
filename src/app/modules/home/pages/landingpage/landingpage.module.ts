import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Page1Component} from './page1/page1.component';
import {LandingpageComponent} from './landingpage.component';
import {MaterialModule} from "../../../../shared/material/material.module";
import { FormsModule } from '@angular/forms';
import {LoginRegistrationService} from "../../../../core/services/login-registration.service";
import {LoginDetailsService} from "../../../../core/services/login-details.service";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {AuthService} from "../../../../core/guards/auth.service";
@NgModule({
  declarations: [
    Page1Component,
    LandingpageComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
  ],
  exports: [
    MaterialModule,
    FormsModule,
    MaterialModule,
    FormsModule,
  ],
  providers:[
    LoginRegistrationService,
    LoginDetailsService,
    AuthService,
  ]
})
export class LandingpageModule {
}
