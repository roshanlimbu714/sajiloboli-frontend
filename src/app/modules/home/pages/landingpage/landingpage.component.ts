import {Component, OnInit} from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import {Router} from '@angular/router';
import {Login, RegistrationData} from '../../../../shared/models/LoginModel';
import {LoginRegistrationService} from '../../../../core/services/login-registration.service';
import {CategoryTagsService} from '../../../../shared/models/categoryTags';
import {LoginDetailsService} from '../../../../core/services/login-details.service';
import {RegisterService} from '../../../../core/services/register.service';


@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss']
})
export class LandingpageComponent implements OnInit {
  hide = true;
  hide2 = true;
  display: any = 1;
  contentItem: any = 1;
  login: Login;
  registration: RegistrationData;
  listofTags: string[] = [];
  listofCategories: string[] = [];


  constructor(private loginService: LoginRegistrationService,
              private router: Router,
              private categoriesService: RegisterService,
              private sendCatsAndTags: CategoryTagsService,
              private loginDetails: LoginDetailsService) {
    this.registration = {};
  }

  loadAllTagsAndCategories() {
    this.categoriesService.getTagsList().subscribe(
      (res) => {
        this.listofTags = res['data'];
      });
    this.categoriesService.getCategoryList().subscribe(
      (res) => {
        this.listofCategories = res['data'];
      });
    this.sendCatsAndTags.sendTagsCategories(this.listofTags, this.listofCategories);
  }

  ngOnInit() {
    this.login = {};

  }

  displayBox(a: any) {
    this.display = a;
  }

  displayContent(a: any) {
    this.contentItem = a;
  }

  onRegisSubmit() {
    this.registration.cpassword = this.registration.password;
    this.loginDetails.sendMessage(this.registration);
    if (this.registration.usertype === 'vendor') {
      this.router.navigate(['register']);
    }
    if (this.registration.usertype === 'customer') {
      this.router.navigate(['registration']);
    }
  }

  onLoginSubmit() {
    console.log('here');
    this.loginService.login(this.login).subscribe(
      (loginResponse) => {
        console.log(loginResponse);
        localStorage.setItem('access_token', loginResponse['access_token']);
        localStorage.setItem('access_token', loginResponse['access_token']);
        localStorage.setItem('token_type', loginResponse['token_type']);
        localStorage.setItem('isnew', loginResponse['isnew']);
        localStorage.setItem('verified', loginResponse['verified']);
        localStorage.setItem('navBarValue', '1');
        console.log('before the navigation');
        const decoded = jwt_decode(loginResponse['access_token']);

        console.log(decoded.scopes[0]);
        localStorage.setItem('userType', decoded.scopes[0]);
        this.router.navigate(['home']);
        console.log('after the navigation');
      },
      (err) => console.log(err)
    );
    this.loadAllTagsAndCategories();
  }

}
