import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {CreatePostService} from '../../../../../core/services/create-post.service';
import {Post} from '../../../../../shared/models/Post';

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {
    isLinear = false;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    listOfTags: any = [];
    tags: any;
    listOfCategories:any=[];
    datePipe: any;
  postModel: Post;
    constructor(private _formBuilder: FormBuilder, private createPostService: CreatePostService) {
    }
    pushTag(tag) {
        this.listOfTags.push(tag);
        console.log(tag);
    }

    remove(tag: string): void {
        let tagindex: number = this.listOfTags.indexOf(tag);
        if (tagindex > -1) {
            this.listOfTags.splice(tagindex, 1);
        }
    }

    ngOnInit() {
        // this.postModel = {};
        this.firstFormGroup = this._formBuilder.group({
            title: ['', Validators.required],
            budget_from: ['', Validators.required],
            budget_to: ['', Validators.required]
        });
        this.secondFormGroup = this._formBuilder.group({
            description: ['', Validators.required],
            expiry_date: ['', Validators.required],
        });

    }

    onPostSubmitClick() {
        this.postModel.tags = this.listOfTags;
        this.postModel.title = this.firstFormGroup.controls['title'].value;
        this.postModel.budget_from = this.firstFormGroup.controls['budget_from'].value;
        this.postModel.budget_to = this.firstFormGroup.controls['budget_to'].value;
        this.postModel.description = this.secondFormGroup.controls['description'].value;
        this.postModel.expiry_date = this.transform(this.secondFormGroup.controls['expiry_date'].value);
        this.postModel.category = this.firstFormGroup.controls['category'].value;

        console.log(this.postModel);
        this.createPostService.createPost(this.postModel).subscribe(res => (res), () => console.log('error'));
    }

    transform(value: any) {
        this.datePipe = new DatePipe('en-US');
        value = this.datePipe.transform(value, 'yyyy-MM-dd');
        return value;
    }

}
