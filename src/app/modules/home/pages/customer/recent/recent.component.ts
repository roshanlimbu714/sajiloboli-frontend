import {Component, OnInit} from '@angular/core';
import {Post} from "../../../../../shared/models/Post";


@Component({
  selector: 'app-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.scss']
})
export class RecentComponent implements OnInit {
  lists = Post;

  constructor() {
  }


  ngOnInit() {
  }

}
