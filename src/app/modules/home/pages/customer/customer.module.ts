import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatePostComponent} from './create-post/create-post.component';
import {RecentComponent} from './recent/recent.component';
import {PostsComponent} from './posts/posts.component';
import {CustomerComponent} from './customer.component';
import {FeedComponent} from './feed/feed.component';
import {MaterialModule} from '../../../../shared/material/material.module';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditPostComponent} from "./edit-post/edit-post.component";

@NgModule({
  declarations: [
    CustomerComponent,
    CreatePostComponent,
    EditPostComponent,
    FeedComponent,
    RecentComponent,
    PostsComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  exports: [
    CustomerComponent,
    MaterialModule,
    ReactiveFormsModule,
  ]
})
export class CustomerModule {
}
