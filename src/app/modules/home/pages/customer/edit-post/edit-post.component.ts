import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Post} from '../../../../../shared/models/Post';
import {EditPostService} from '../../../../../core/services/edit-post.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
  post: Post = {};

  constructor(private_FormBuilder: FormBuilder, private editPostService: EditPostService) {
  }

  ngOnInit() {
  }

  onPostSubmitClick() {

    //this.post.tags = split => array push => array assign thispost.tags
    // this.post.tags =  this.tags.split(' ');
    // console.log(this.post);
    this.editPostService.editPost(this.post).subscribe((postResponse) => console.log(postResponse['message']),
      (error) => console.log(error));
  }
}
