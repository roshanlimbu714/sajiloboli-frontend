import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, Sort} from '@angular/material';
import {EditItemsComponent} from '../edit-items/edit-items.component';
import {UserRequest} from './user.model';
import {UserRequestService} from './user-request.service';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-user-request',
  templateUrl: './user-request.component.html',
  styleUrls: ['./user-request.component.scss']
})
export class UserRequestComponent implements OnInit {
  userDataList: UserRequest[] = [];
  userDataList2: UserRequest[] = [];
  userDataList3: UserRequest[] = [];
  sortedData: UserRequest[] = [];
  sortedData2: UserRequest[] = [];
  sortedData3: UserRequest[] = [];
  userDataSend: UserRequest;
  changeContent: any = 1;
  datePipe: any;

  constructor(public dialog: MatDialog, public userRequestService: UserRequestService) {
    // this.sortedData = this.userDataList.slice();
    // this.sortedData2 = this.userDataList2.slice();
    // this.sortedData3 = this.userDataList3.slice();
  }

  transform(value: any) {
    this.datePipe = new DatePipe('en-US');
    value = this.datePipe.transform(value, 'yyyy-MM-dd');
    return value;
  }

  loadUsers() {
    this.userRequestService.getUnverifiedUsers().subscribe(
      (res) => {
        this.userDataList = res['data'];
        this.sortedData = this.userDataList.slice();
        console.log(this.sortedData);
      }
    )
    this.userRequestService.getAllUsers().subscribe(
      (res) => {
        this.userDataList2 = res['data'];
        this.sortedData2 = this.userDataList2.slice();
        console.log(this.sortedData2);
      }
    )
    this.userRequestService.getRejectedUsers().subscribe(
      (res) => {
        this.userDataList3 = res['data'];
        this.sortedData3 = this.userDataList3.slice();
        console.log(this.sortedData3);
      }
    )
  }

  approveuser(a: any) {
    this.userRequestService.approveUser(a);
  }

  rejectuser(a: any) {
    this.userRequestService.rejectUser(a);
  }

  booleanValueReturn(a: boolean) {
    if (a) {
      return 'Yes';
    }
    else
      return 'No';
  }

  public ngOnInit() {
    this.loadUsers();

  }

  changeContents(a: any) {
    this.changeContent = a;
  }

  viewDetails(a: UserRequest): void {
    this.userDataSend = a;
    const dialogRef = this.dialog.open(EditItemsComponent, {data: {user: this.userDataSend}});
  }

  sortData(sort: Sort) {
    const data = this.userDataList.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id':
          return compare(a.id, b.id, isAsc);
        case 'name':
          return compare(a.name, b.name, isAsc);
        case 'emailverification':
          return compare(a.emailVerified, b.emailVerified, isAsc);
        case 'location':
          return compare(a.member_detail.contact.permanentaddress, b.member_detail.contact.permanentaddress, isAsc);
        case 'email':
          return compare(a.email, b.email, isAsc);
        case 'dateregistered':
          return compare(this.transform(a.createdAt), this.transform(b.createdAt), isAsc);
        default:
          return 0;
      }
    });
  }
}


function compare(a: number | string | boolean, b: number | string | boolean, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

