import { TestBed, inject } from '@angular/core/testing';

import { Home\admin\userRequest\UserRequestService } from './home\admin\user-request\user-request.service';

describe('Home\admin\userRequest\UserRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Home\admin\userRequest\UserRequestService]
    });
  });

  it('should be created', inject([Home\admin\userRequest\UserRequestService], (service: Home\admin\userRequest\UserRequestService) => {
    expect(service).toBeTruthy();
  }));
});
