export class PostRequest {
  public id?: number;
  public title?: string;
  public budget_from?: number;
  public budget_to?: number;
  public description?: any;
  public status?: string;
  public expiry_date?: number;
  public category?: string;
  public tags?: any;
  public updated_at?: any;

  constructor(id?: number,
              title?: string,
              budget_from?: number,
              budget_to?: number,
              description?: any,
              status?: string,
              expiry_date?: number,
              category?: string,
              tags?: any,
              updated_at?: any) {
    this.id = id ? id : null;
    this.title = title ? title : null;
    this.budget_from = budget_from ? budget_from : null;
    this.budget_to = budget_to ? budget_to:null;
    this.description = description ? description : null;
    this.status = status ? status : null;
    this.expiry_date = expiry_date ? expiry_date : null;
    this.category = category ? category : null;
    this.tags = tags ? tags : null;
    this.updated_at = updated_at ? updated_at : null;
  }
}


