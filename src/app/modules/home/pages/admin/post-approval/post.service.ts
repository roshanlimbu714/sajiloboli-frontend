import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private url = 'http://localhost:8000/api/admin/';
  getUnverifiedPosts() {
    return this.http.get(this.url + 'showUnverifiedPosts');
  }
  approvePosts(id: number) {
    return this.http.get(this.url + 'verifyPosts/' + id);
  }
  constructor(private http: HttpClient) { }
}
