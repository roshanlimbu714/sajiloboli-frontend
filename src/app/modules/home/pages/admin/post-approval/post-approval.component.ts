import {Component, OnInit} from '@angular/core';
import {PostService} from "./post.service";
import {PostRequest} from "./post.model";
import {DatePipe} from "@angular/common";
import {Sort} from "@angular/material";

@Component({
  selector: 'app-post-approval',
  templateUrl: './post-approval.component.html',
  styleUrls: ['./post-approval.component.scss']
})
export class PostApprovalComponent implements OnInit {
  unverifiedPosts: PostRequest[] = [];
  sortedData: PostRequest[] = [];
  datePipe: any;
  PostDataSend: PostRequest;
  changeContent:any=1;
  constructor(public postService: PostService) {
  }

  loadPosts() {
    this.postService.getUnverifiedPosts().subscribe(
      (res) => {
        this.unverifiedPosts = res['data'];
        this.sortedData = this.unverifiedPosts.slice();
        console.log(this.sortedData);
      }
    )
  }

  transform(value: any) {
    this.datePipe = new DatePipe('en-US');
    value = this.datePipe.transform(value, 'yyyy-MM-dd');
    return value;
  }

  sortData(sort: Sort) {
    const data = this.unverifiedPosts.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id':
          return compare(a.id, b.id, isAsc);
        case 'title':
          return compare(a.title, b.title, isAsc);
        case 'budgetfrom':
          return compare(a.budget_from, b.budget_from, isAsc);
        case 'budgetto':
          return compare(a.budget_to, b.budget_to, isAsc);
        case 'expirydate':
          return compare(a.expiry_date, b.expiry_date, isAsc);
        case 'updatedat':
          return compare(a.updated_at, b.updated_at, isAsc);
        default:
          return 0;
      }
    });
  }

  booleanValueReturn(a: boolean) {
    if (a) {
      return 'Yes';
    }
    else
      return 'No';
  }

  ngOnInit() {
    this.loadPosts();
  }

}

function compare(a: number | string | boolean, b: number | string | boolean, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
