import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminComponent} from "./admin.component";
import {EditItemsComponent} from "./edit-items/edit-items.component";
import {FeedbackComponent} from "./feedback/feedback.component";
import {NegotiationComponent} from "./negotiation/negotiation.component";
import {PostApprovalComponent} from "./post-approval/post-approval.component";
import {SettingsComponent} from "./settings/settings.component";
import {UserRequestComponent} from "./user-request/user-request.component";
import {WelcomeAdminComponent} from "./welcome-admin/welcome-admin.component";
import {MaterialModule} from "../../../../shared/material/material.module";

@NgModule({
  declarations: [
    AdminComponent,
    EditItemsComponent,
    FeedbackComponent,
    NegotiationComponent,
    PostApprovalComponent,
    SettingsComponent,
    UserRequestComponent,
    WelcomeAdminComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[
    MaterialModule
  ]
})
export class AdminModule { }
